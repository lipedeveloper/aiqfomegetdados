<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}


	public function insertDB(){
		$insert = $this->input->post("dados");



		$count = 0;		
		foreach ($insert as $key => $value) {
			unset($value['virtual_favoritos']);
			$r = $this->db->insert("user",$value);	
			$count = $key;		
			 
		}


		$obj = array(
		  'quantidade' => $count+1
		);
		echo json_encode($obj);

	}
}
